FROM alpine:latest as builder

ENV S3DEPLOY_VERSION 2.11.0
ENV S3DEPLOY_SHA d3566a6a56735ea5710cf22c35f8f411164e6efc22ded98743a1d7a5a21c740e

RUN apk --no-cache add ca-certificates
WORKDIR /root/
ADD https://github.com/bep/s3deploy/releases/download/v${S3DEPLOY_VERSION}/s3deploy_${S3DEPLOY_VERSION}_linux-amd64.tar.gz s3deploy.tar.gz
RUN echo "${S3DEPLOY_SHA}  s3deploy.tar.gz" | sha256sum -c && tar xvzf s3deploy.tar.gz

FROM alpine:latest
RUN apk --no-cache add ca-certificates
COPY --from=builder /root/s3deploy /usr/local/bin/s3deploy
